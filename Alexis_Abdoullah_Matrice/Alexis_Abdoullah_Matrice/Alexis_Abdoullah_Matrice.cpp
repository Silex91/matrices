// Alexis_Abdoullah_Matrice.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include "CMatrice.h"
using namespace std;
#define TESTS_CONSTRUCTEURS 1;

#ifdef MAIN


int main()
{
    std::cout << "Hello World!\n";
	cout << " Ok C'est parti mon kiki !" << endl;
}

#endif // MAIN

#ifdef TESTS_CONSTRUCTEURS
int main()
{
	cout << "HelloWorld !" << endl;
	CMatrice<int> M1;
	CMatrice<int> M2(3, 2);

	cout << "M1 : " << M1.MATGetNbLignes() << " lignes et " << M1.MATGetNbColonnes() << "colonnes" << endl;
	cout << "M2 : " << M2.MATGetNbLignes() << " lignes et " << M2.MATGetNbColonnes() << "colonnes" << endl;

	int **tab = new int*[3];
	for (int i = 0; i < 3; i++) tab[i] = new int[2];
	tab[0][0] = 16;
	tab[0][1] = 12;
	tab[1][0] = 4;
	tab[1][1] = 18;
	tab[2][0] = 6;
	tab[2][1] = 7;
	cout << "test 1\n";
	M2.MATSetTableau(tab);
	cout << "test 2\n";
	int ** tab2 = M2.MATGetTableau();
	cout << tab2[0][0] << " " << tab2[0][1] << " " << tab2[1][0] << " " << tab2[1][1] << " " << tab2[2][0] << " " << tab2[2][1] << "\n";

	CMatrice<int> M3(M2);
	int ** tab3 = M3.MATGetTableau();
	cout << tab3[0][0] << " " << tab3[0][1] << " " << tab3[1][0] << " " << tab3[1][1] << " " << tab3[2][0] << " " << tab3[2][1] << "\n";

}
#endif // TESTS_CONSTRUCTEURS

#ifdef TESTS_SETTERS_GETTERS
int main()
{
	cout << "HelloWorld !" << endl;

}
#endif // TESTS_SETTERS_GETTERS


// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
