#pragma once
#include<iostream>
#include<stdio.h>
using namespace std;

template <typename T>
class CMatrice
{
private :
	unsigned int uiMATNbLignes;
	unsigned int uiMATNbColonnes;
	T ** pptMATTableau2D;

public :
/*------------------------------------------------------
--------Constructeurs-----------------------------------
-------------------------------------------------------*/
	CMatrice();
	CMatrice(unsigned int, unsigned int);
	CMatrice(unsigned int, unsigned int, T**);
	CMatrice(const CMatrice&);

/*------------------------------------------------------
--------Getters et Setters------------------------------
-------------------------------------------------------*/
	unsigned int MATGetNbLignes();
	void MATSetNbLignes(unsigned int);
	unsigned int MATGetNbColonnes();
	void MATSetNbColonnes(unsigned int);
	T** MATGetTableau();
	void MATSetTableau(T**);

/*------------------------------------------------------
--------Méthodes----------------------------------------
-------------------------------------------------------*/

};


/*---------------------------------------------------------------------------------------------------
-----------------------Constructeurs-----------------------------------------------------------------
----------------------------------------------------------------------------------------------------*/
template <typename T>
CMatrice<T>::CMatrice()
	: uiMATNbLignes(0), uiMATNbColonnes(0), pptMATTableau2D(NULL)
{}

template <typename T>
CMatrice<T>::CMatrice(unsigned int uiNbLigneParam, unsigned int uiNbColonneParam)
	: uiMATNbLignes(uiNbLigneParam), uiMATNbColonnes(uiNbColonneParam)
{
	pptMATTableau2D = new T*[uiNbLigneParam];
	for (short unsigned int suiIndice = 0; suiIndice < uiNbColonneParam; suiIndice++)
		pptMATTableau2D[suiIndice] = new T[uiNbColonneParam];
}

template <typename T>
CMatrice<T>::CMatrice(unsigned int uiNbLigneParam, unsigned int uiNbColonneParam, T** templateParam)
{
	this(uiNbLigneParam, uiNbColonneParam);

	for (short unsigned int suiIndiceLigne = 0; suiIndiceLigne < uiNbLigneParam; suiIndiceLigne++)
	{
		for (short unsigned int suiIndiceColonne = 0; suiIndiceColonne < uiNbColonneParam; suiIndiceColonne++)
			pptMATTableau2D[suiIndiceLigne][suiIndiceColonne] = templateParam[suiIndiceLigne][suiIndiceColonne];
	}
}

template <typename T>
CMatrice<T>::CMatrice(const CMatrice& matMatriceParam)
{
	/*this(matMatriceParam.uiMATNbLignes, matMatriceParam.uiMATNbColonnes , matMatriceParam.pptMATTableau2D);
		Ne fonctionne pas pour une raison inconnue */

	uiMATNbLignes = matMatriceParam.uiMATNbLignes;
	uiMATNbColonnes = matMatriceParam.uiMATNbColonnes;

	pptMATTableau2D = new T*[uiMATNbLignes];
	for (unsigned int uiIndiceLigne = 0; uiIndiceLigne < uiMATNbLignes; uiIndiceLigne++)
	{
		pptMATTableau2D[uiIndiceLigne] = new T[uiMATNbColonnes];
		for (unsigned int uiIndiceColonne = 0; uiIndiceColonne < uiMATNbColonnes; uiIndiceColonne++)
		{
			pptMATTableau2D[uiIndiceLigne][uiIndiceColonne] = matMatriceParam.pptMATTableau2D[uiIndiceLigne][uiIndiceColonne];
		}
	}
}


/*---------------------------------------------------------------------------------------------------
-----------------------Getters et Setters------------------------------------------------------------
----------------------------------------------------------------------------------------------------*/
template <typename T>
unsigned int CMatrice<T>::MATGetNbLignes()
{
	return uiMATNbLignes;
}

template <typename T>
void CMatrice<T>::MATSetNbLignes(unsigned int uiParam)
{
	uiMATNbLignes = uiParam;
}

template <typename T>
unsigned int CMatrice<T>::MATGetNbColonnes()
{
	return uiMATNbColonnes;
}

template <typename T>
void CMatrice<T>::MATSetNbColonnes(unsigned int uiParam)
{
	uiMATNbColonnes = uiParam;
}

template<typename T>
T** CMatrice<T>::MATGetTableau()
{
	if (pptMATTableau2D == NULL || !uiMATNbColonnes || !uiMATNbLignes)
	{
		/* LEVER UNE EXCEPTION */
		return NULL;
	}

	T ** tableau = new T*[uiMATNbLignes];
	for (unsigned int uiIndiceLigne = 0; uiIndiceLigne < uiMATNbLignes; uiIndiceLigne++)
	{
		tableau[uiIndiceLigne] = new T[uiMATNbColonnes];
		for (unsigned int uiIndiceColonne = 0; uiIndiceColonne < uiMATNbColonnes; uiIndiceColonne++)
		{
			tableau[uiIndiceLigne][uiIndiceColonne] = pptMATTableau2D[uiIndiceLigne][uiIndiceColonne];
		}
	}
	return tableau;
}

/* Préconditions : le tableau en param doit avoir la taille de la matrice (MATNbLigne et MATNbColonne) */
template<typename T>
void CMatrice<T>::MATSetTableau(T**tTableauParam)
{
	if (tTableauParam == NULL || !uiMATNbColonnes || !uiMATNbLignes)
	{
		/* LEVER UNE EXCEPTION */
		pptMATTableau2D = NULL;
	}
	else
	{
		pptMATTableau2D = new T*[uiMATNbLignes];
		for (unsigned int uiIndiceLigne = 0; uiIndiceLigne < uiMATNbLignes; uiIndiceLigne++)
		{
			pptMATTableau2D[uiIndiceLigne] = new T(uiMATNbColonnes);
			for (unsigned int uiIndiceColonne = 0; uiIndiceColonne < uiMATNbColonnes; uiIndiceColonne++)
			{
				pptMATTableau2D[uiIndiceLigne][uiIndiceColonne] = tTableauParam[uiIndiceLigne][uiIndiceColonne];
			}
		}
	}
}

/*---------------------------------------------------------------------------------------------------
-----------------------Méthodes----------------------------------------------------------------------
----------------------------------------------------------------------------------------------------*/
